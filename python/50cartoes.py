import random

"""
   Gera uma sequencia numerica para mega sena.
"""

def gera_sequencia(num = 50):
    for n in range(0,num):
		return random.sample(range(1,61), 6)

gera_sequencia