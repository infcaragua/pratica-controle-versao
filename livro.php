<?php
	class Livro
	{
		private $nome;
		private $isbn;
		private $genero;

		function __construct($nome,$isbn,$genero){
			$this->setNome($nome);
			$this->setIsbn($isbn);
			$this->setGenero($genero);
		}

		function getNome() {
			return $this->nome;
		}
		function setNome($nome){
			$this->nome=$nome;
		}

		function getIsbn() {
			return $this->isbn;
		}
		function setIsbn($isbn){
			$this->isbn=$isbn;
		}

		function getGenero() {
			return $this->genero;
		}
		function setGenero($genero){
			$this->genero=$genero;
		}
	}

?>